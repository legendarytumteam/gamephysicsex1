#pragma once
#include "Simulator.h"
#include <vector>
#include "MassPoint.h"
#include "Spring.h"

class MassSpringSystem {
public:
	std::vector<MassPoint*> pointList;
	std::vector<Spring*> springList;

	void computeElasticForces(std::vector<MassPoint*>, std::vector<Spring*>);
	void integratePositionsandvelocity(float, int);

	MassSpringSystem();
	~MassSpringSystem();

	void addMassPoint(Vec3 p, Vec3 v, float m, float d, bool fixed);

	void addSpring(int p1, int p2, float k, float len);

	void clear();

	void updateStep(float dt, int);

	void applyForce(Vec3 force);

	void applyGravity();
};