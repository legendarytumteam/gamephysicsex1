#include "Spring.h"

Spring* Spring::clone()
{
	Spring* copy = new Spring();
	copy->currentLength = this->currentLength;
	copy->initialLength = this->initialLength;
	copy->point1 = this->point1;
	copy->point2 = this->point2;
	copy->stiffness = this->stiffness;
	return copy;
}

Spring::Spring()
{
	currentLength = 0;
	initialLength = 0;
	point1 = 0;
	point2 = 0;
	stiffness = 0;
}