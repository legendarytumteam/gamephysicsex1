#pragma once
class Utils
{
public:
	Utils();
	~Utils();

	Point2D* get2dPoint(Vec3 pos, Mat4 viewMatrix, Mat4 projectionMatrix, int width, int height);
};

