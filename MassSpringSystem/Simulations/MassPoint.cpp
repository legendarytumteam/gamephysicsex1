#include "MassPoint.h"

void MassPoint::clearForce()
{
	force = 0;
}

void MassPoint::applyForce(const Vec3& f)
{
	force += f;
}

void MassPoint::addGravity()
{
	Vec3 gravity(0.0f, 0.0f, -9.8f);
	force += mass*gravity;
}

MassPoint* MassPoint::clone()
{
	MassPoint *copy = new MassPoint();
	copy->damping = this->damping;
	copy->force = this->force;
	copy->isFixed = this->isFixed;
	copy->mass = this->mass;
	copy->position = this->position;
	copy->velocity = this->velocity;
	return copy;
}