#include "RigidBodySystemSimulator.h"

RigidBodySystemSimulator::RigidBodySystemSimulator() : Simulator() {
	m_iTestCase = 0;

	m_externalForce = Vec3();
	m_pRigidBodySystem = new RigidBodySystem();
}

const char * RigidBodySystemSimulator::getTestCasesStr() {
	return "Demo1";
}

void RigidBodySystemSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	this->DUC = DUC;
	switch (m_iTestCase)
	{
	case 0:break;
	default:break;
	}
}

void RigidBodySystemSimulator::reset() {
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
}



void RigidBodySystemSimulator::notifyCaseChanged(int testCase)
{
	m_iTestCase = testCase;

	m_pRigidBodySystem->clear();
	switch (m_iTestCase)
	{
	case 0:
		this->addRigidBody(Vec3(0.1, -0.1, 0.15), Vec3(0.2, 0.3, 0.4), 1);
		for (int i = 0; i < 8; i++) {
			/*if (i == 0 || i== 1)
				m_pRigidBodySystem->getRigidBodyAt(0)->applyPointForce(i, Vec3(0, 50, 0));
			else
				m_pRigidBodySystem->getRigidBodyAt(0)->applyPointForce(i, Vec3(50, 0, 0));*/
		}
	
		// Debug
		/*std::cout << "Pos : " << m_pRigidBodySystem->rigidBodies[0]->position
			<< "\nRot : " << m_pRigidBodySystem->rigidBodies[0]->orientation
			<< "\nScale : " << m_pRigidBodySystem->rigidBodies[0]->scale
			<< "\nRotMat : " << m_pRigidBodySystem->rigidBodies[0]->orientation.getRotMat()
			<< "\nworld pos : + " << m_pRigidBodySystem->rigidBodies[0]->getWorldMatrix();*/
		break;
	default:
		break;
	}
}


void RigidBodySystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext) {
	DUC->setUpLighting(Vec3(0, 0, 0), 0.4*Vec3(1, 1, 1), 2000.0, Vec3(0.5, 0.5, 0.5));

	std::vector<RigidBody*> & rigidBodies = m_pRigidBodySystem->getRigidBodies();
	for (auto& rigidBody : rigidBodies) {
		DUC->drawRigidBody(rigidBody->getWorldMatrix());
	}
}


void RigidBodySystemSimulator::externalForcesCalculations(float timeElapsed)
{
	// Apply accumulated mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
	if (m_trackmouse.x != 0 || m_trackmouse.y != 0)
	{
		// Calcuate camera directions in world space
		Mat4 m = Mat4(DUC->g_camera.GetViewMatrix());
		m = m.inverse();
		Vec3 camRightWorld = Vec3(g_XMIdentityR0);
		camRightWorld = m.transformVectorNormal(camRightWorld);
		Vec3 camUpWorld = Vec3(g_XMIdentityR1);
		camUpWorld = m.transformVectorNormal(camUpWorld);

		//std::cout << "m : " << m << "\n\nright : " << camRightWorld << "\n\nup : " << camUpWorld << "\n ----------\n\n\n";
		std::cout << "track mouse : " << m_oldtrackmouse.x << ", " << m_oldtrackmouse.y << "\n";
		
		
		Vec3 mouseForce = camUpWorld * -1 * m_trackmouse.y  + camRightWorld * m_trackmouse.x;
		std::vector<RigidBody*> & rigidBodies = m_pRigidBodySystem->getRigidBodies();
		for (auto& rigidBody : rigidBodies) {
			Point2D* objPos = get2dPoint(rigidBody->getPosition(), DUC->g_camera.GetViewMatrix(), DUC->g_camera.GetProjMatrix(), 1280, 780);
			std::cout << "old Pos : " << rigidBody->getPosition() << "     objPos : " << objPos->x << ", " << objPos->y << "\n---\n\n";
			if (checksIfInRange(&m_oldtrackmouse, objPos))
				std::cout << "worked";
			rigidBody->applyForce(mouseForce);
		}


		// Reset accumulated mouse deltas
		m_trackmouse.x = m_trackmouse.y = 0;
	}
}

void RigidBodySystemSimulator::simulateTimestep(float timeStep) 
{
	externalForcesCalculations(timeStep);
	m_pRigidBodySystem->updateStep(timeStep);
}

void RigidBodySystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x += x - m_oldtrackmouse.x;
	m_trackmouse.y += y - m_oldtrackmouse.y;
}

void RigidBodySystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
};

//------------------------------------------------------
//EXTRA FUNCTIONS
//------------------------------------------------------

int RigidBodySystemSimulator::getNumberOfRigidBodies() {
	switch (m_iTestCase)
	{
	default: return 0;
		break;
	}
}

Vec3 RigidBodySystemSimulator::getPositionOfRigidBody(int i) {
	switch (m_iTestCase)
	{
	default: return Vec3(0,0,0);
		break;
	}
}

Vec3 RigidBodySystemSimulator::getLinearVelocityOfRigidBody(int i) {
	switch (m_iTestCase)
	{
	default: return Vec3(0, 0, 0);
		break;
	}
}

Vec3 RigidBodySystemSimulator::getAngularVelocityOfRigidBody(int i) {
	switch (m_iTestCase)
	{
	default: return Vec3(0, 0, 0);
		break;
	}
}

void RigidBodySystemSimulator::applyForceOnBody(int i, Vec3 loc, Vec3 force) {
	m_pRigidBodySystem->applyForceOnBody(i, loc, force);
}

void RigidBodySystemSimulator::addRigidBody(Vec3 position, Vec3 size, int mass) {
	m_pRigidBodySystem->addRigidBody(position, size, mass);
}

void RigidBodySystemSimulator::setOrientationOf(int i, Quat orientation) {
	m_pRigidBodySystem->setOrientationOf(i, orientation);
}

void RigidBodySystemSimulator::setVelocityOf(int i, Vec3 velocity) {
	m_pRigidBodySystem->setVelocityOf(i, velocity);
}

Point2D* RigidBodySystemSimulator::get2dPoint(Vec3 pos, Mat4 viewMatrix, Mat4 projectionMatrix, int width, int height)
{
	//Mat4 inversedViewMat = viewMatrix.inverse();
	Mat4 viewProjMat = projectionMatrix * viewMatrix;
	Vec3 transfPos = viewProjMat.transformVectorNormal(pos);
	int x = (int) roundf(((transfPos.x + 1) / 2.0f) * width);
	int y = (int) roundf(((1 - transfPos.y) / 2.0f) * height);

	Point2D* p = new Point2D();
	p->x = x;
	p->y = y;

	return p;
}

bool RigidBodySystemSimulator::checksIfInRange(Point2D* click, Point2D* object)
{
	float dist = sqrt(pow(click->x - object->x, 2) + pow(click->y - object->y, 2));
	std::cout << "distance : " << dist << "\n";
	if (dist <= CLICKRANGE)
		return true;
	else
		return false;
}







