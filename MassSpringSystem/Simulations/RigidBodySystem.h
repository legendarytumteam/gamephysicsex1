#pragma once
#include "SimpleMath.h"
#include <vector>
#include "RigidBody.h"


class RigidBodySystem {
private:
	std::vector<RigidBody*> rigidBodies;
public:
	RigidBodySystem();
	~RigidBodySystem();

	int getNrRigidBodies();
	RigidBody * getRigidBodyAt(int index);
	std::vector<RigidBody*> & getRigidBodies();

	void addRigidBody(Vec3 position, Vec3 size, float mass);

	void applyForceOnBody(int i, Vec3 loc, Vec3 force);
	void setOrientationOf(int i, Quat quaternion);
	void setVelocityOf(int i, Vec3 velocity);

	void clear();
	void updateStep(float dt);
	void applyGravity();
};