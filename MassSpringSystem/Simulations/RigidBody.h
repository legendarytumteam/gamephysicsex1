#pragma once
#define RIGID_BODY_NR_EDGES 8

#include "Simulator.h"
#include <math.h>

class RigidBody {
private:
	// transform variables
	Vec3 centerMass;
	Quat orientation;
	Vec3 scale;

	// rigid body variables
	float mass;
	Real pointMass[RIGID_BODY_NR_EDGES];

	Vec3 pointToCenterMass[RIGID_BODY_NR_EDGES];
	Vec3 linearVelocity;
	Vec3 force;
	Vec3 pointForce[RIGID_BODY_NR_EDGES];
	Mat4 inverseInertiaTensor;
	Vec3 angularMomentum;
	Vec3 angularVelocity;
public:
	RigidBody();
	RigidBody(Vec3 position, Vec3 scale, float mass);

	// functions
	void clearForce();
	void addGravity();
	void applyForce(const Vec3& f);
	void applyPointForce(int index, const Vec3& f);
	void updateStep(float dt);
	RigidBody* clone();
	
	Vec3 getPosition();

	Mat4 getWorldMatrix();
};

