#include "RigidBodySystem.h"

RigidBodySystem::RigidBodySystem() {
}

RigidBodySystem::~RigidBodySystem() {
	int nrRigidBodies = rigidBodies.size();
	for (int i = 0; i < nrRigidBodies; i++) {
		delete rigidBodies[i];
	}
}

int RigidBodySystem::getNrRigidBodies() {
	return rigidBodies.size();
}

RigidBody * RigidBodySystem::getRigidBodyAt(int index) {
	return rigidBodies[index];
}

std::vector<RigidBody*> & RigidBodySystem::getRigidBodies() {
	return rigidBodies;
}

void RigidBodySystem::applyForceOnBody(int i, Vec3 loc, Vec3 force)
{
	// TODO
}

void RigidBodySystem::addRigidBody(Vec3 p, Vec3 s, float m)
{
	rigidBodies.push_back(new RigidBody(p, s, m));
}

void RigidBodySystem::setOrientationOf(int i, Quat quaternion)
{
	// TODO
}

void RigidBodySystem::setVelocityOf(int i, Vec3 velocity)
{
	// TODO
}


void RigidBodySystem::clear() 
{
	for (int i = 0; i < rigidBodies.size(); i++) {
		delete rigidBodies[i];
	}
	rigidBodies.clear();
}

void RigidBodySystem::updateStep(float dt) {
	for (auto& rigidBody : rigidBodies) {
		rigidBody->updateStep(dt);
	}
}

void RigidBodySystem::applyGravity() 
{
	for (auto& rigidBody : rigidBodies) {
		rigidBody->addGravity();
	}
}


