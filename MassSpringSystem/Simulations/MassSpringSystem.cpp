#include "MassSpringSystem.h"
#include <math.h>
#include <functional>
#include <algorithm>
#include <iterator>

void MassSpringSystem::addMassPoint(Vec3 p, Vec3 v, float m, float d, bool fixed)
{
	pointList.push_back(new MassPoint(p, v, m, d, fixed));
}

void MassSpringSystem::addSpring(int p1, int p2, float k, float len)
{
	springList.push_back(new Spring(p1, p2, k, len));
}

void MassSpringSystem::clear()
{
	int i;
	for (i = 0; i<pointList.size(); i++) {
		delete pointList[i];
	}
	pointList.clear();
	for (i = 0; i<springList.size(); i++) {
		delete springList[i];
	}
	springList.clear();
}

void MassSpringSystem::updateStep(float dt, int method)
{
	for (auto& point : pointList) {
		point->clearForce();
		if (point->isFixed) {
			continue;
		}
		//point->addGravity();
	}

	computeElasticForces(pointList, springList);

	integratePositionsandvelocity(dt, method);
}

void MassSpringSystem::integratePositionsandvelocity(float dt, int method)
{
	switch (method)
	{
	    case 0: //EULER
		{
			for (auto& point : pointList) {
				point->position += point->velocity * dt;
				point->velocity += (point->force / point->mass)*dt;			
			}
			break;
		}
		case 2: //MIDPOINT
		{ 
			std::vector<MassPoint*> temppointList;
			std::transform(pointList.begin(), pointList.end(), std::back_inserter(temppointList), std::mem_fun(&MassPoint::clone));
			std::vector<Spring*> tempspringList;
			std::transform(springList.begin(), springList.end(), std::back_inserter(tempspringList), std::mem_fun(&Spring::clone));
			for (int i = 0; i < pointList.size(); i++) {
				temppointList[i]->position += (temppointList[i]->velocity * dt)/2;
				temppointList[i]->velocity += ((temppointList[i]->force / temppointList[i]->mass)*dt)/2;
			}
			for (auto& point : temppointList) {
				point->clearForce();
				if (point->isFixed) {
					continue;
				}
			}
			computeElasticForces(temppointList, tempspringList);
			int i = 0;
			for (auto& point : pointList) {
				point->position += temppointList[i]->velocity * dt;
				point->velocity += (temppointList[i]->force / temppointList[i]->mass)*dt;
				i++;
			}
			break;

		}
		default:
			break;
	}

}


void MassSpringSystem::applyForce(Vec3 force)
{
	for (auto& point : pointList) {
		point->applyForce(force);
	}
}

void MassSpringSystem::applyGravity()
{
	for (auto& point : pointList) {
		point->addGravity();
	}
}

void MassSpringSystem::computeElasticForces(std::vector<MassPoint*> pointList, std::vector<Spring*> springList)
{
	for (auto& spring : springList) {
		MassPoint *point1 = pointList[spring->point1];
		MassPoint *point2 = pointList[spring->point2];
		float dx = point1->position.x - point2->position.x;
		float dy = point1->position.y - point2->position.y;
		float dz = point1->position.z - point2->position.z;
		spring->currentLength = sqrt(dx*dx + dy*dy + dz*dz);
		Vec3 force = spring->stiffness*(spring->currentLength - spring->initialLength);
		force.x *= -(dx / spring->currentLength);
		force.y *= -(dy / spring->currentLength);
		force.z *= -(dz / spring->currentLength);
		if (!point1->isFixed)
		{
			point1->force += force;
		}
		if (!point2->isFixed)
		{
			point2->force -= force;
		}
	}
}

MassSpringSystem::~MassSpringSystem()
{
	for (int i = 0; i<pointList.size(); i++) {
		delete pointList[i];
	}
	for (int i = 0; i<springList.size(); i++) {
		delete springList[i];
	}
}

MassSpringSystem::MassSpringSystem()
{

}
