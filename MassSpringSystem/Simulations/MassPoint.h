#pragma once
#include "Simulator.h"

class MassPoint
{
public:
	MassPoint(Vec3 p, Vec3 v, float m, float d, bool f) :position(p), velocity(v), mass(m), damping(d), isFixed(f)
	{
		force = 0;
	}
	MassPoint() :position(Vec3()), velocity(Vec3()), mass(0), damping(0), isFixed(0)
	{
		force = 0;
	}

	void clearForce();
	void addGravity();
	void applyForce(const Vec3& f);
	Vec3 calculateForce();
	MassPoint* clone();
	
	Vec3 position;
	Vec3 velocity;
	Vec3 force;
	float mass;
	float damping;
	bool isFixed;
};
