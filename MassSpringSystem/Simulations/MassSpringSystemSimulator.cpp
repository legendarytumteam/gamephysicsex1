#include "MassSpringSystemSimulator.h"

MassSpringSystemSimulator::MassSpringSystemSimulator()
{
	m_fMass=0;
	m_fStiffness=0;
	m_fDamping=0;
	m_iIntegrator=0;
	m_iTestCase = 0;
	m_externalForce=Vec3();
	mss = new MassSpringSystem();
}

const char * MassSpringSystemSimulator::getTestCasesStr() {
	return "Demo1, Demo2, Demo3, Demo4";
}

const char * MassSpringSystemSimulator::getIntegratorStr() {
	return "EULER, LEAPFROG, MIDPOINT";
}

void MassSpringSystemSimulator::reset() {
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
}

void MassSpringSystemSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	this->DUC = DUC;
	switch (m_iTestCase)
	{
	case 0:break;
	case 1:break;
	case 2:break;
	case 3:
	{
		TwType TW_TYPE_INT = TwDefineEnumFromString("Integrator", getIntegratorStr());
		TwAddVarRW(DUC->g_pTweakBar, "Integrator", TW_TYPE_INT, &m_iIntegrator, "");
		//TwAddVarRW(DUC->g_pTweakBar, "Integrator", TW_TYPE_INT32, &m_integrator, "min=1");
		//TwAddVarRW(DUC->g_pTweakBar, "Sphere Size", TW_TYPE_FLOAT, &m_fSphereSize, "min=0.01 step=0.01");
		break;
	}
	default:break;
	}
}

void MassSpringSystemSimulator::notifyCaseChanged(int testCase)
{
	m_iTestCase = testCase;
	switch (m_iTestCase)
	{
	case 0:
	{
		cout << "Demo1 !\n";
		clear();
		setMass(10.0f);
		setStiffness(40.0f);
		setDampingFactor(0.0f);
		setIntegrator(EULER);
		addMassPoint(Vec3(0.0f, 0.0f, 0.0f), Vec3(-1.0f, 0.0f, 0.0f), false);
		addMassPoint(Vec3(0.0f, 2.0f, 0.0f), Vec3(1.0f, 0.0f, 0.0f), false);
		addSpring(0, 1, 1.0f);
		applyExternalForce(Vec3(0.0f, 0.0f, 0.0f));
		float timeStep = 0.1f;
		//externalForcesCalculations(timeStep);
		//simulateTimestep(timeStep);
		mss->updateStep(timeStep, m_iIntegrator);

		std::cout << "Demo" << m_iTestCase + 1 << std::endl;
		std::cout << "Timestep :" << timeStep << std::endl;
		std::cout << "Integration Method(0:EULER, 1:MIDPOINT) " << m_iIntegrator << std::endl;
		for (int i = 0; i < getNumberOfMassPoints(); i++)
			std::cout << "Position : " << getPositionOfMassPoint(i) << "  Velocities: " << getVelocityOfMassPoint(i) << std::endl;


		break;
	}
	case 1:
	{
		cout << "Demo2 !\n";
		clear();
		setMass(10.0f);
		setStiffness(40.0f);
		setDampingFactor(0.0f);
		setIntegrator(EULER);
		addMassPoint(Vec3(0.0f, 0.0f, 0.0f), Vec3(-1.0f, 0.0f, 0.0f), false);
		addMassPoint(Vec3(0.0f, 2.0f, 0.0f), Vec3(1.0f, 0.0f, 0.0f), false);
		addSpring(0, 1, 1.0f);
		applyExternalForce(Vec3(0.0f, 0.0f, 0.0f));
		float timeStep = 0.005f;
		//externalForcesCalculations(timeStep);
		//simulateTimestep(timeStep);
		mss->updateStep(timeStep, m_iIntegrator);
		oldintegrator = m_iIntegrator;

		std::cout << "Demo" << m_iTestCase + 1 << std::endl;
		std::cout << "Timestep :" << timeStep << std::endl;
		std::cout << "Integration Method(0:EULER, 1:MIDPOINT) " << m_iIntegrator << std::endl;
		for (int i = 0; i < getNumberOfMassPoints(); i++)
			std::cout << "Position : " << getPositionOfMassPoint(i) << "  Velocities: " << getVelocityOfMassPoint(i) << std::endl;

		break;
	}
	case 2:
	{
		cout << "Demo 3!\n";
		clear();
		setMass(10.0f);
		setStiffness(40.0f);
		setDampingFactor(0.0f);
		setIntegrator(MIDPOINT);
		addMassPoint(Vec3(0.0f, 0.0f, 0.0f), Vec3(-1.0f, 0.0f, 0.0f), false);
		addMassPoint(Vec3(0.0f, 2.0f, 0.0f), Vec3(1.0f, 0.0f, 0.0f), false);
		addSpring(0, 1, 1.0f);
		applyExternalForce(Vec3(0.0f, 0.0f, 0.0f));
		float timeStep = 0.005f;
		//externalForcesCalculations(timeStep);
		//simulateTimestep(timeStep);
		mss->updateStep(timeStep, m_iIntegrator);

		std::cout << "Demo" << m_iTestCase + 1 << std::endl;
		std::cout << "Timestep :" << timeStep << std::endl;
		std::cout << "Integration Method(0:EULER, 1:MIDPOINT) " << m_iIntegrator << std::endl;
		for (int i = 0; i < getNumberOfMassPoints(); i++)
			std::cout << "Position : " << getPositionOfMassPoint(i) << "  Velocities: " << getVelocityOfMassPoint(i) << std::endl;


		break;
	}
	case 3:
	{
		clear();
		setMass(10.0f);
		setStiffness(40.0f);
		setDampingFactor(0.0f);
		setIntegrator(MIDPOINT);
		float min = -2000.0f;
		float max = 2000.0f;
		for (int i = 0; i < 10; i++) {
			float randx = (min + (rand() % (int)(max - min + 1)))/1000;
			float randy = (min + (rand() % (int)(max - min + 1)))/1000;
			float randz = (min + (rand() % (int)(max - min + 1)))/1000;
			float randvx = (min + (rand() % (int)(max - min + 1))) / 1000;
			float randvy = (min + (rand() % (int)(max - min + 1))) / 1000;
			float randvz = (min + (rand() % (int)(max - min + 1))) / 1000;
			addMassPoint(Vec3(randx, randy, randz), Vec3(randvx, randvy, randvz), false);
		}
		for(int j=0; j<= 5; j++)
		for (int i = 1; i <= 4; i++) {
			MassPoint *point1 = mss->pointList[j];
			MassPoint *point2 = mss->pointList[j+i];
			float dx = point1->position.x - point2->position.x;
			float dy = point1->position.y - point2->position.y;
			float dz = point1->position.z - point2->position.z;
			float currentLength = sqrt(dx*dx + dy*dy + dz*dz);
			addSpring(j, j+i , currentLength);
		}
		MassPoint *point1 = mss->pointList[0];
		MassPoint *point2 = mss->pointList[9];
		float dx = point1->position.x - point2->position.x;
		float dy = point1->position.y - point2->position.y;
		float dz = point1->position.z - point2->position.z;
		float currentLength = sqrt(dx*dx + dy*dy + dz*dz);
		addSpring(0, 9, currentLength);

		applyExternalForce(Vec3(0.0f, 0.0f, 0.0f));
		mss->applyGravity();
		float timeStep = 0.1f;
		//externalForcesCalculations(timeStep);
		//simulateTimestep(timeStep);
		mss->updateStep(timeStep, m_iIntegrator);

		std::cout << "Demo" << m_iTestCase + 1 << std::endl;
		std::cout << "Timestep :" << timeStep << std::endl;
		std::cout << "Integration Method(0:EULER, 1:MIDPOINT) " << m_iIntegrator << std::endl;
		for (int i = 0; i < getNumberOfMassPoints(); i++)
			std::cout << "Position : " << getPositionOfMassPoint(i) << "  Velocities: " << getVelocityOfMassPoint(i) << std::endl;

		break;

	}
	default:
		clear();
		cout << "Empty Test!\n";
		break;
	}
}

void MassSpringSystemSimulator::externalForcesCalculations(float timeElapsed)
{
	// Apply accumulated mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
	if (m_trackmouse.x != 0 || m_trackmouse.y != 0)
	{
		// Calcuate camera directions in world space
		Mat4 m = Mat4(DUC->g_camera.GetViewMatrix());
		m = m.inverse();
		Vec3 camRightWorld = Vec3(g_XMIdentityR0);
		camRightWorld = m.transformVectorNormal(camRightWorld);
		Vec3 camUpWorld = Vec3(g_XMIdentityR1);
		camUpWorld = m.transformVectorNormal(camUpWorld);

		// Add accumulated mouse deltas to movable object pos

		float speedScale = 0.001f;
		for (int i = 0; i < mss->pointList.size(); i++) {
			mss->pointList[i]->position += speedScale * (float)m_trackmouse.x * camRightWorld;
			mss->pointList[i]->position += -speedScale * (float)m_trackmouse.y * camUpWorld;
		}
		//m_vfMovableObjectPos += speedScale * (float)m_trackmouse.x * camRightWorld;
		//m_vfMovableObjectPos += -speedScale * (float)m_trackmouse.y * camUpWorld;

		// Reset accumulated mouse deltas
		m_trackmouse.x = m_trackmouse.y = 0;
	}
}

void MassSpringSystemSimulator::simulateTimestep(float timeStep)
{
	// update current setup for each frame
	mss->updateStep(timeStep, m_iIntegrator);
	switch (m_iTestCase)
	{
	case 3:
	{
		if (oldintegrator != m_iIntegrator) {
			oldintegrator = m_iIntegrator;
			std::cout << "Demo" << m_iTestCase + 1 << std::endl;
			std::cout << "Timestep :" << timeStep << std::endl;
			std::cout << "Integration Method(0:EULER, 2:MIDPOINT) " << m_iIntegrator << std::endl;
			for (int i = 0; i < getNumberOfMassPoints(); i++)
				std::cout << "Position : " << getPositionOfMassPoint(i) << "  Velocities: " << getVelocityOfMassPoint(i) << std::endl;
		}
		break;
	}
	default:
		break;
	}
}

void MassSpringSystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	Vec3 springColor = Vec3(0.1, 0.3, 0.4);
	DUC->setUpLighting(springColor, springColor, 100, springColor);

	for (auto i = 0; i != mss->pointList.size(); i++) {
		DUC->drawSphere(mss->pointList[i]->position, mss->pointList[i]->mass*MASS_POINT_SCALING_FACTOR);
	}

	for (auto i = 0; i != mss->springList.size(); i++) {
		DUC->beginLine();
		int index1 = mss->springList[i]->point1;
		int index2 = mss->springList[i]->point2;
		DUC->drawLine(mss->pointList[index1]->position, SPRING_COLOR, mss->pointList[index2]->position, SPRING_COLOR);
		DUC->endLine();
	}
}

void MassSpringSystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x += x - m_oldtrackmouse.x;
	m_trackmouse.y += y - m_oldtrackmouse.y;
}

void MassSpringSystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
};



void MassSpringSystemSimulator::setIntegrator(int integrator)
{
	m_iIntegrator = integrator;
}
void MassSpringSystemSimulator::setMass(float mass)
{
	m_fMass = mass;
}
void MassSpringSystemSimulator::setStiffness(float stiffness)
{
	m_fStiffness = stiffness;
}
void MassSpringSystemSimulator::setDampingFactor(float damping)
{
	m_fDamping = damping;
}
int MassSpringSystemSimulator::addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed)
{
	mss->addMassPoint(position, Velocity, m_fMass, m_fDamping, isFixed);
	return 0;
}
void MassSpringSystemSimulator::addSpring(int masspoint1, int masspoint2, float initialLength)
{
	mss->addSpring(masspoint1, masspoint2, m_fStiffness, initialLength);
}
int MassSpringSystemSimulator::getNumberOfMassPoints()
{
	return mss->pointList.size();
}
int MassSpringSystemSimulator::getNumberOfSprings()
{
	return mss->springList.size();
}
Vec3 MassSpringSystemSimulator::getPositionOfMassPoint(int index)
{
	return mss->pointList[index]->position;
}
Vec3 MassSpringSystemSimulator::getVelocityOfMassPoint(int index)
{
	return mss->pointList[index]->velocity;
}
void MassSpringSystemSimulator::applyExternalForce(Vec3 force)
{
	mss->applyForce(force);
}
void MassSpringSystemSimulator::clear()
{
	mss->clear();
}

