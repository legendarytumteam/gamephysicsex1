#pragma once
#include "Simulator.h"
#include "MassPoint.h"

class Spring
{
public:
	Spring(int p1, int p2, float k, float len) :point1(p1), point2(p2), stiffness(k), initialLength(len)
	{
		currentLength = initialLength;
	}
	int point1;
	int point2;
	float stiffness;
	float initialLength;
	float currentLength;
	Spring* clone();
	Spring();
};
