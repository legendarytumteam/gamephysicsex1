#include "RigidBody.h"

RigidBody::RigidBody() : RigidBody(Vec3(0,0,0), Vec3(1, 1, 1), 1) {
};

RigidBody::RigidBody(Vec3 position, Vec3 scale, float mass) : centerMass(position), orientation(Quat(0,1,0,0)), scale(scale), linearVelocity(Vec3()), angularVelocity(Vec3()), mass(mass) {
	clearForce();

	//Compute edge mass mi
	for (int i = 0; i < RIGID_BODY_NR_EDGES; i++) {
		pointMass[i] = mass / RIGID_BODY_NR_EDGES;
	}
	//Compute relative edge positions xi'
	pointToCenterMass[0] = Vec3(0,0,0);
	pointToCenterMass[1] = Vec3(scale.x,0,0);
	pointToCenterMass[2] = Vec3(0, scale.y, 0);
	pointToCenterMass[3] = Vec3(scale.x, scale.y, 0);
	pointToCenterMass[4] = Vec3(0, 0, scale.z);
	pointToCenterMass[5] = Vec3(scale.x, 0, scale.z);
	pointToCenterMass[6] = Vec3(0, scale.y, scale.z);
	pointToCenterMass[7] = Vec3(scale.x, scale.y, scale.z);
	//Compute relative center mass xcm'
	centerMass = 0;
	for (int i = 0; i < RIGID_BODY_NR_EDGES; i++) {
		centerMass += (pointMass[i] / mass) * pointToCenterMass[i];
	}
	//Compute point to center mass xi
	for (int i = 0; i < RIGID_BODY_NR_EDGES; i++) {
		pointToCenterMass[i] -= centerMass;
	}
	//Compute global center mass xcm
	centerMass += position;

	//Initialize remaining variables
	linearVelocity = Vec3(0, 0, 0); //vc
	angularMomentum = Vec3(0, 0, 0); //L
	
	//Compute initial inverse inertia tensor I0^-1
	for (int i = 0; i < RIGID_BODY_NR_EDGES; i++) {
		inverseInertiaTensor.value[0][0] += pointMass[i] * (pow(pointToCenterMass[i].y,2) + pow(pointToCenterMass[i].z, 2));
		inverseInertiaTensor.value[1][1] += pointMass[i] * (pow(pointToCenterMass[i].x, 2) + pow(pointToCenterMass[i].z, 2));
		inverseInertiaTensor.value[2][2] += pointMass[i] * (pow(pointToCenterMass[i].x, 2) + pow(pointToCenterMass[i].y, 2));
		inverseInertiaTensor.value[0][1] -= pointMass[i] * (pointToCenterMass[i].x*pointToCenterMass[i].y);
		inverseInertiaTensor.value[1][2] -= pointMass[i] * (pointToCenterMass[i].y*pointToCenterMass[i].z);
		inverseInertiaTensor.value[0][2] -= pointMass[i] * (pointToCenterMass[i].x*pointToCenterMass[i].z);
	}
	inverseInertiaTensor.value[1][0] = inverseInertiaTensor.value[0][1];
	inverseInertiaTensor.value[2][1] = inverseInertiaTensor.value[1][2];
	inverseInertiaTensor.value[2][0] = inverseInertiaTensor.value[0][2];
	inverseInertiaTensor.value[3][3] = 1;

	inverseInertiaTensor = inverseInertiaTensor.inverse();
	//Compute inverse inertia tensor I^-1
	Mat4 rotR = orientation.getRotMat();
	Mat4 rotRT = orientation.getRotMat();
	rotRT.transpose();
	inverseInertiaTensor = rotR * inverseInertiaTensor * rotRT;
	//Compute initial angular velocity
	angularVelocity = inverseInertiaTensor * angularMomentum;
};

void RigidBody::clearForce() {
	force = 0;
	for (int i = 0; i < RIGID_BODY_NR_EDGES; i++) {
		pointForce[i] = 0;
	}
}

void RigidBody::addGravity() {
	Vec3 gravity(0.0f, 0.0f, -9.8f);
	force += mass*gravity;
}

void RigidBody::applyForce(const Vec3 & f) 
{
	force += f;
}

void RigidBody::applyPointForce(int index, const Vec3& f) {
	pointForce[index] = f;
}

RigidBody * RigidBody::clone() 
{
	RigidBody *copy = new RigidBody();
	copy->centerMass = this->centerMass;
	copy->orientation = this->orientation;
	copy->linearVelocity = this->linearVelocity;
	copy->angularVelocity = this->angularVelocity;
	copy->mass = this->mass;
	return copy;
}

Vec3 RigidBody::getPosition()
{
	return centerMass;
}


Mat4 RigidBody::getWorldMatrix() {
	Mat4 scaleMat = Mat4(
		scale.x, 0, 0, 0,
		0, scale.y, 0, 0,
		0, 0, scale.z, 0,
		0, 0, 0, 1);
	Mat4 transMat = Mat4(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		centerMass.x- scale.x/2, centerMass.y-scale.y/2, centerMass.z-scale.z/2, 1);
	
	return scaleMat * orientation.getRotMat() * transMat;
}

void RigidBody::updateStep(float dt) {
	//compute overall force F
	for (int i = 0; i < RIGID_BODY_NR_EDGES; i++) {
		force += pointForce[i];
	}
	
	//compute torque q
	Vec3 torque = Vec3(0,0,0);
	for (int i = 0; i < RIGID_BODY_NR_EDGES; i++) {
		torque += GamePhysics::cross(pointToCenterMass[i], pointForce[i]);
	}
	
	//euler step
	centerMass += dt * linearVelocity;
	linearVelocity += dt*force / mass;

	//quaternion
	Quat zeroAngularVelocity = Quat(0, angularVelocity.x, angularVelocity.y, angularVelocity.z);
	Real step = (dt / 2);
	orientation +=  (zeroAngularVelocity * orientation) * step;
	orientation /= orientation.norm();

	//update angular momentum
	angularMomentum += dt* torque;

	//update inverse inertia tensor
	Mat4 rotR = orientation.getRotMat();
	Mat4 rotRT = orientation.getRotMat();
	rotRT.transpose();
	inverseInertiaTensor = rotR * inverseInertiaTensor * rotRT;

	//update angular velocity
	angularVelocity = inverseInertiaTensor * angularMomentum;

	clearForce();
}


